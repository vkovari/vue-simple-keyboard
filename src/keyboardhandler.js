
import { reactive } from "@vue/reactivity"

var keyboardhandler = {
    state: reactive({
        inputs: [],
        selectedInput: null,
        show: false,
        typing: false,
        inputName: '',
        layoutType: "basic",
        layouts: {
            basic: null,
            numeric: {
                default: ["1 2 3", "4 5 6", "7 8 9", "{shift} 0 _", "{bksp}"],
                shift: ["! / #", "$ % ^", "& * (", "{shift} ) +", "{bksp}"]
            }
        }
    }),
    onClick(event) {
        if (!event.target.classList.contains("input")) {
            this.show = true
        }
    },
    onFocus(event) {
        if (event.target.classList.contains("input")) {
            this.show = true
            this.stage.inputName = event.target.id;
            if (event.target.dataset.layout) {
               this.state.layoutType = event.target.dataset.layout;
            }
        }        
    },
    setInputs(inputs) {
        this.state.inputs = inputs
    },
    setShow(show) {
        this.state.show = show
    },
    setSelectedInput(selectedInput) {
        this.state.selectedInput = selectedInput
    },
    setTyping(typing) {
        this.state.typing = typing
    },
    setInputName(inputName) {
        this.state.inputName = inputName
    },
    setInput(input) {
        this.state.inputs[this.state.inputName] = input
    }
}

export default keyboardhandler