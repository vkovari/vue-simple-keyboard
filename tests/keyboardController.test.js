
import SimpleKeyboard from 'simple-keyboard'

let keyboard, view, controller

beforeEach(function () {
    keyboard = new SimpleKeyboard()
    view = {}
    controller = new KeyboardController(view)
})


test('init', function () {
    expect(controller.keyboard).not.toEqual(null)
    expect(controller.view).toEqual(view)
})



function KeyboardController(view) {
    this.keyboard = new SimpleKeyboard({
        onChange: input => this.onChange(),
        onKeyPress: input => this.onKeyPress()
    })
    this.view = view
}

KeyboardController.prototype.onChange = function (input) {
    console.log(input)
}

KeyboardController.prototype.onKeyPress = function (button) {
    console.log(button)
}